#!/bin/sh -eu

#### PUBLIC ####

main()(
	PACKAGES="client server utility";

	_main "${@}";
);

#### PRIVATE ####

_echo_stdin(){
	local _PREFIX;

	local _line;

	_PREFIX="${1:-}";

	while IFS="" read -r _line;
	do
		printf '%s%s\n' "${_PREFIX}" "${_line}";
	done;

	return 0;
};

_usage(){
	local _package;

	_echo_stdin <<\
───────────────────────────────────────────────────────────────────────────────
Usage
-----

    ${SELF} [-?|-h]

Format and vet each of the Golang packages

$(for _package in ${PACKAGES}; do printf '*   `%s`\n\n' "${_package}"; done;)

and each Golang source file in the root directory of the project.
───────────────────────────────────────────────────────────────────────────────

	return 0;
}

_help(){
	_echo_stdin <<\
───────────────────────────────────────────────────────────────────────────────
$(_usage)

Parameters
----------

*   \`-?\` – Write a short usage text to the standard output and exit.

*   \`-h\` – Write a longer help text to the standard output and exit.
───────────────────────────────────────────────────────────────────────────────

	return 0;
}

_fail(){
	_echo_stdin <<\
───────────────────────────────────────────────────────────────────────────────
Error
-----

$(for _line in "${@}"; do echo "${_line}"; done;)

$(_usage)
───────────────────────────────────────────────────────────────────────────────

	return 1;
};

_main(){
	local ROOT="${0%/*}";
	local SELF="${0##*/}";

	(
		local _MODULE;
		local _ITEMS;

		local OPTARG;
		local OPTIND;
		local OPTION;

		local _item;
		local _output;
		local _parameter;
		local _status;

		for _parameter in "${@}";
		do
			case "${_parameter}" in
				("-?") _usage && return 0;;
				("-h") _help && return 0;;
			esac;
		done;

		OPTIND=0;
		while getopts ":" OPTION;
		do
			case "${OPTION}" in
				("?") _fail "The command-line parameter \`-${OPTARG}\` is not valid.";;
				(":") _fail "The command-line parameter \`-${OPTARG}\` requires an argument.";;
			esac;
		done;

		if test ${#} -gt 0;
		then
			_fail "The command-line parameters \`${*}\` are not valid.";
		fi;

		cd "${ROOT}";

		_MODULE=$(go list -m);
		_ITEMS="";
		for _item in ${PACKAGES};
		do
			_ITEMS="${_ITEMS} ${_MODULE}/${_item}";
		done;
		for _item in *".go";
		do
			test -f "${_item}" &&_ITEMS="${_ITEMS} ${_item}";
		done;

		_status=0;

		echo "Format sources:";
		echo "";
		for _item in ${_ITEMS}
		do
			if _output=$(go fmt "${_item}" 2>&1)
			then
				if test "${_output}";
				then
					printf '*   `%s`\n\n' "${_output}";
					_status=1;
				fi;
			else
				printf '*   `%s`\n\n' "${_item}";
				echo "${_output}" | _echo_stdin "        ";
				printf '\n';
				_status=1;
			fi;
		done;

		echo "Vet sources:";
		echo "";
		for _item in ${_ITEMS}
		do
			if ! _output=$(go vet "${_item}" 2>&1);
			then
				printf '*   `%s`\n\n' "${_item}";
				echo "${_output}" | _echo_stdin "        ";
				printf '\n';
				_status=1;
			fi;
		done;

		return ${_status};
	);
};

main "${@}";
