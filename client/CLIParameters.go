package client

import (
	"flag"
	"strings"

	"gitlab.com/paul_john_king/http_tools/utility"
)

type cliParameters struct {
	utility.CLIParameters
	count    int
	delay    int
	interval int
	url      string
}

func CLIParameters(usage string) (parameters *cliParameters) {
	parameters = new(cliParameters)

	flag.Usage = parameters.Usage(usage)
	flag.IntVar(
		&parameters.count,
		"count",
		1,
		"Send `«count»` requests then exit.",
	)
	flag.IntVar(
		&parameters.delay,
		"delay",
		0,
		"Wait `«count»` seconds before sending the first request.",
	)
	flag.IntVar(
		&parameters.interval,
		"interval",
		0,
		"Wait `«count»` seconds before sending each subsequent request.",
	)
	flag.StringVar(
		&parameters.url,
		"url",
		"http://localhost:80/",
		"Send requests to the URL `«url»`.",
	)
	flag.Parse()

	if flag.NArg() > 0 {
		parameters.Fail("parameters invalid: %s\n", strings.Join(flag.Args(), " "))
	}

	return
}

func (parameters *cliParameters) GetCount() int {
	return parameters.count
}

func (parameters *cliParameters) GetDelay() int {
	return parameters.delay
}

func (parameters *cliParameters) GetInterval() int {
	return parameters.interval
}

func (parameters *cliParameters) GetURL() string {
	return parameters.url
}
