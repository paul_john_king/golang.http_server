package client

type Client interface {
	Channel() chan Data
	Run()
}
