package client

import (
	"sort"
	"strings"
)

type Data struct {
	Error   error
	Status  string
	Header  map[string][]string
	Trailer map[string][]string
	Body    []byte
}

func (data *Data) String() string {
	var (
		builder strings.Builder
		item    string
		key     string
		keys    []string
	)

	builder.WriteString("Response:\n")

	builder.WriteString("  Status: ")
	builder.WriteString(data.Status)
	builder.WriteString("\n")

	builder.WriteString("  Header:\n")
	keys = nil
	for key = range data.Header {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key = range keys {
		builder.WriteString("    ")
		builder.WriteString(key)
		builder.WriteString(":\n")
		for _, item = range data.Header[key] {
			builder.WriteString("    - ")
			builder.WriteString(item)
			builder.WriteString("\n")
		}
	}

	builder.WriteString("  Trailer:\n")
	keys = nil
	for key = range data.Trailer {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key = range keys {
		builder.WriteString("    ")
		builder.WriteString(key)
		builder.WriteString(":\n")
		for _, item = range data.Trailer[key] {
			builder.WriteString("    - ")
			builder.WriteString(item)
			builder.WriteString("\n")
		}
	}

	builder.WriteString("  Body:\n")
	for _, item = range strings.Split(strings.TrimSpace(string(data.Body)), "\n") {
		builder.WriteString("    ")
		builder.WriteString(item)
		builder.WriteString("\n")
	}

	return builder.String()
}
