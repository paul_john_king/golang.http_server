package client

type Parameters interface {
	GetCount() int
	GetDelay() int
	GetInterval() int
	GetURL() string
}
