package client

import (
	"io"
	"net/http"
	"time"
)

type simpleClient struct {
	http.Client
	dataChannel chan Data
	count       int
	delay       int
	interval    int
	url         string
}

func SimpleClient(parameters Parameters) (client *simpleClient) {
	client = new(simpleClient)
	client.dataChannel = make(chan Data)

	client.count = parameters.GetCount()
	client.delay = parameters.GetDelay()
	client.interval = parameters.GetInterval()
	client.url = parameters.GetURL()

	return
}

func (client *simpleClient) Channel() chan Data {
	return client.dataChannel
}

func (client *simpleClient) Run() {
	var (
		data     Data
		index    int
		response *http.Response
	)

	if client.count <= 0 {
		close(client.dataChannel)
		return
	}

	if client.delay > 0 {
		time.Sleep(time.Duration(client.delay) * time.Second)
	}

	index = 0
	for {
		response, data.Error = client.Get(client.url)
		if data.Error == nil {
			defer response.Body.Close()

			data.Status = response.Status
			data.Header = response.Header
			data.Trailer = response.Trailer
			data.Body, data.Error = io.ReadAll(response.Body)
		}

		client.dataChannel <- data

		index++

		if index == client.count {
			close(client.dataChannel)
			return
		}

		if client.interval > 0 {
			time.Sleep(time.Duration(client.interval) * time.Second)
		}
	}
}
