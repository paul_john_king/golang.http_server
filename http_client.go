///usr/bin/env go run -race "${0}" "${@}"; exit;

package main

import (
	"log"
	"os"

	http_client "gitlab.com/paul_john_king/http_tools/client"
)

func main() {
	var (
		channel    chan http_client.Data
		client     http_client.Client
		data       http_client.Data
		open       bool
		parameters http_client.Parameters
	)

	parameters = http_client.CLIParameters(`
Run a simple HTTP client that sends ´GET´ requests to an HTTP server.
`)

	client = http_client.SimpleClient(parameters)
	channel = client.Channel()

	log.Printf(
		"Starting:\n  Count: %d\n  Delay: %d\n  Interval: %d\n  URL: %s\n  Process ID: %d\n",
		parameters.GetCount(),
		parameters.GetDelay(),
		parameters.GetInterval(),
		parameters.GetURL(),
		os.Getpid(),
	)

	go client.Run()

	for {
		data, open = <-channel
		if open {
			if data.Error == nil {
				log.Printf("%s", data.String())
			} else {
				log.Printf("Error: %s\n", data.Error)
			}
		} else {
			if data.Error == nil {
				log.Printf("Stopping:\n")
				return
			} else {
				log.Fatalf("Error: %s\n", data.Error)
			}
		}
	}
}
