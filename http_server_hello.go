///usr/bin/env go run -race "${0}" "${@}"; exit;

package main

import (
	"log"
	"os"
	"syscall"

	http_server "gitlab.com/paul_john_king/http_tools/server"
)

func main() {
	var (
		channel    chan http_server.Data
		data       http_server.Data
		open       bool
		parameters http_server.Parameters
		server     http_server.Server
	)

	parameters = http_server.CLIParameters(`
Run a simple HTTP server that returns an HTML5 "Hello, World" response to each
client request.  Send the signal ´SIGINT´ to the server to stop it.
`)

	server = http_server.SimpleServer(
		parameters,
		map[string]http_server.Handler{
			"/": http_server.HelloWorldHandler(),
		},
		syscall.SIGINT,
	)
	channel = server.Channel()

	log.Printf(
		"Starting:\n  Address: %s\n  Process ID: %d\n",
		parameters.GetAddress(),
		os.Getpid(),
	)

	go server.Run()

	for {
		data, open = <-channel
		if open {
			if data.Error == nil {
				log.Printf("%s", data.String())
			} else {
				log.Printf("Error: %s\n", data.Error)
			}
		} else {
			if data.Error == nil {
				log.Printf("Stopping:\n")
				return
			} else {
				log.Fatalf("Error: %s\n", data.Error)
			}
		}
	}
}
