package server

import (
	"flag"
	"strings"

	"gitlab.com/paul_john_king/http_tools/utility"
)

type cliParameters struct {
	utility.CLIParameters
	address string
}

func CLIParameters(usage string) (parameters *cliParameters) {
	parameters = new(cliParameters)

	flag.Usage = parameters.Usage(usage)
	flag.StringVar(
		&parameters.address,
		"address",
		"0.0.0.0:80",
		"Receive requests at the address `«address»`.",
	)
	flag.Parse()

	if flag.NArg() > 0 {
		parameters.Fail("parameters invalid: %s\n", strings.Join(flag.Args(), " "))
	}

	return
}

func (parameters *cliParameters) GetAddress() string {
	return parameters.address
}
