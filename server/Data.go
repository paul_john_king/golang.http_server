package server

import (
	"sort"
	"strings"
)

type Data struct {
	Error         error
	RemoteAddress string
	UserAgent     string
	Protocol      string
	Method        string
	URL           string
	Header        map[string][]string
	Trailer       map[string][]string
}

func (data *Data) String() string {
	var (
		builder strings.Builder
		item    string
		key     string
		keys    []string
	)

	builder.WriteString("Request:\n")

	builder.WriteString("  Remote Address: ")
	builder.WriteString(data.RemoteAddress)
	builder.WriteString("\n")

	builder.WriteString("  User Agent: ")
	builder.WriteString(data.UserAgent)
	builder.WriteString("\n")

	builder.WriteString("  Protocol: ")
	builder.WriteString(data.Protocol)
	builder.WriteString("\n")

	builder.WriteString("  Method: ")
	builder.WriteString(data.Method)
	builder.WriteString("\n")

	builder.WriteString("  URL: ")
	builder.WriteString(data.URL)
	builder.WriteString("\n")

	builder.WriteString("  Header:\n")
	keys = nil
	for key = range data.Header {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key = range keys {
		builder.WriteString("    ")
		builder.WriteString(key)
		builder.WriteString(":\n")
		for _, item = range data.Header[key] {
			builder.WriteString("    - ")
			builder.WriteString(item)
			builder.WriteString("\n")
		}
	}

	builder.WriteString("  Trailer:\n")
	keys = nil
	for key = range data.Trailer {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for _, key = range keys {
		builder.WriteString("    ")
		builder.WriteString(key)
		builder.WriteString(":\n")
		for _, item = range data.Trailer[key] {
			builder.WriteString("    - ")
			builder.WriteString(item)
			builder.WriteString("\n")
		}
	}

	return builder.String()
}
