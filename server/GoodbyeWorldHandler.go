package server

import (
	"net/http"
)

type goodbyeWorldHandler struct {
	contentType string
	content     []byte
}

func GoodbyeWorldHandler() (handler *goodbyeWorldHandler) {
	handler = new(goodbyeWorldHandler)
	handler.contentType = "text/html; charset=utf-8"
	handler.content = []byte(`<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Goodbye, World</title>
  </head>
  <body>
    <div>
      <p>Goodbye, World</p>
    </div>
  </body>
</html>
`)

	return
}

func (handler *goodbyeWorldHandler) ServeHTTP(
	writer http.ResponseWriter,
	request *http.Request,
	channel chan Data,
) {
	var (
		data Data
	)

	data.RemoteAddress = request.RemoteAddr
	data.UserAgent = request.UserAgent()
	data.Protocol = request.Proto
	data.Method = request.Method
	data.URL = request.URL.String()
	data.Header = request.Header
	data.Trailer = request.Trailer

	channel <- data

	writer.Header().Set("Content-Type", handler.contentType)
	writer.WriteHeader(http.StatusOK)
	writer.Write(handler.content)

	return
}
