package server

import (
	"net/http"
)

type helloWorldHandler struct {
	contentType string
	content     []byte
}

func HelloWorldHandler() (handler *helloWorldHandler) {
	handler = new(helloWorldHandler)
	handler.contentType = "text/html; charset=utf-8"
	handler.content = []byte(`<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hello, World</title>
  </head>
  <body>
    <div>
      <p>Hello, World</p>
    </div>
  </body>
</html>
`)

	return
}

func (handler *helloWorldHandler) ServeHTTP(
	writer http.ResponseWriter,
	request *http.Request,
	channel chan Data,
) {
	var (
		data Data
	)

	data.RemoteAddress = request.RemoteAddr
	data.UserAgent = request.UserAgent()
	data.Protocol = request.Proto
	data.Method = request.Method
	data.URL = request.URL.String()
	data.Header = request.Header
	data.Trailer = request.Trailer

	channel <- data

	writer.Header().Set("Content-Type", handler.contentType)
	writer.WriteHeader(http.StatusOK)
	writer.Write(handler.content)

	return
}
