package server

type Parameters interface {
	GetAddress() string
}
