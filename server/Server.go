package server

type Server interface {
	Channel() chan Data
	Run()
}
