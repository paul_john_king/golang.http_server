package server

import (
	"net/http"
	"os"
	"os/signal"
)

type simpleServer struct {
	http.Server
	dataChannel   chan Data
	signalChannel chan os.Signal
}

func SimpleServer(
	parameters Parameters,
	handlers map[string]Handler,
	signals ...os.Signal,
) (server *simpleServer) {
	var (
		mux *http.ServeMux
	)

	server = new(simpleServer)
	server.dataChannel = make(chan Data)
	server.signalChannel = make(chan os.Signal)

	server.Addr = parameters.GetAddress()

	mux = new(http.ServeMux)
	for path, handler := range handlers {
		mux.HandleFunc(
			path,
			func(writer http.ResponseWriter, request *http.Request) {
				handler.ServeHTTP(writer, request, server.dataChannel)
				return
			},
		)
	}
	server.Handler = mux

	for _, sig := range signals {
		signal.Notify(server.signalChannel, sig)
	}

	return
}

func (server *simpleServer) Channel() chan Data {
	return server.dataChannel
}

func (server *simpleServer) Run() {
	go func() {
		var (
			data Data
		)

		data.Error = server.ListenAndServe()
		if data.Error != nil && data.Error != http.ErrServerClosed {
			server.dataChannel <- data
			server.Close()
			close(server.dataChannel)
			return
		}
	}()

	<-server.signalChannel
	server.Close()
	close(server.dataChannel)
	return
}
