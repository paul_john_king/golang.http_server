package utility

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"unicode/utf8"
)

type CLIParameters struct{}

func (parameters *CLIParameters) getMarkdown(in string) string {
	return strings.ReplaceAll(
		strings.TrimSpace(in),
		"´",
		"`",
	)
}

func (parameters *CLIParameters) getTitle(text string) string {
	return fmt.Sprintf(
		"%s\n%s\n",
		text,
		strings.Repeat("-", utf8.RuneCountInString(text)),
	)
}

func (parameters *CLIParameters) getUsage(text string) string {
	return fmt.Sprintf(
		"    %s [options]\n\n%s\n",
		filepath.Base(os.Args[0]),
		parameters.getMarkdown(text),
	)
}

func (parameters *CLIParameters) Usage(text string) func() {
	return func() {
		var (
			builder strings.Builder
			key     string
		)

		builder.WriteString(parameters.getTitle("Usage"))
		builder.WriteString("\n")
		builder.WriteString(parameters.getUsage(text))
		builder.WriteString("\n")

		builder.WriteString(parameters.getTitle("Options"))
		builder.WriteString("\n")
		flag.VisitAll(
			func(f *flag.Flag) {
				key, _ = flag.UnquoteUsage(f)

				builder.WriteString("*   `-")
				builder.WriteString(f.Name)
				if len(key) > 0 {
					builder.WriteString(" ")
					builder.WriteString(key)
				}
				builder.WriteString("`\n\n")

				builder.WriteString("    ")
				builder.WriteString(f.Usage)
				builder.WriteString("\n\n")

				builder.WriteString("    Default: `")
				builder.WriteString(f.DefValue)
				builder.WriteString("`\n\n")
			},
		)

		fmt.Fprintln(flag.CommandLine.Output(), builder.String())

		return
	}
}

func (parameters *CLIParameters) Fail(format string, arguments ...interface{}) {
	fmt.Fprintf(flag.CommandLine.Output(), format, arguments...)
	flag.Usage()
	os.Exit(2)

	return
}
